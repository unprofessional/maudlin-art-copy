import React from 'react'
import GalleryContainer from '../pages/Gallery/GalleryContainer'

function Gallery() {
    return (
        <div className="gallery">

                <GalleryContainer />
        </div>
    )
}

export default Gallery